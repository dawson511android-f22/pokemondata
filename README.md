# PokemonData



## Context

As a brief reprieve from the project, we want to make a small application that presents information about Pokemon. This application will allow the user to enter the name of a particular Pokemon, and the application will present the user with an image of the Pokemon, along with relevant data. 

## Assignment Setup and Submission

This lab is to be done individually. Go to the GitLab repository for the Pokefacts starter code. Fork it into your own rep (please pick a reasonable repo name). You may then give access to your teacher (@abodza, or abodzay@dawsoncollege.qc.ca with the developer role). Development should be done on a separate development branch, when you are ready to submit the assignment, create a merge request from your dev branch onto your main branch and assign it to me (@abodza). 
The assignment is due on November 28th, 2022 at 23:59. Late assignments will not be accepted. If you will be unable to meet this deadline due to exceptional circumstances, notify me as soon as possible. 


## PokeFacts
The goal of this application is to retrieve data from https://pokeapi.co/ to describe and present information about Pokemon. To access information from this consumption-only API, you must use the HTTP GET request method.  To avoid overloading the server with requests, we will be using a local database to cache any information that we retrieve. This will both make the application faster for the user of the application due to fewer web requests, while also being friendly to the operators of the API. 

Full details about this API can be found in it’s documentation https://pokeapi.co/docs/v2#info
This API has information about any detail you may have wanted to know about any aspect of the Pokemon video games, but for this assignment we will be focusing specifically on the pokemon:

We formulate GET requests for pokemon information from the API as follows:
https://pokeapi.co/api/v2/pokemon/{id or name}/

We can provide either the ID of the pokemon (a unique numeric identifier) or the pokemon’s name in order to retrieve a JSON response describing that pokemon. This means that if we want to get information about Pokemon #1, Bulbasaur, we can request it in 2 ways:
https://pokeapi.co/api/v2/pokemon/1/ OR https://pokeapi.co/api/v2/pokemon/bulbasaur/ 

The retrieved JSON description of the Pokemon has a lot of information, but we don’t need all of it. To simplify this process somewhat, we have provided you with a PokeJSONSimplifier, which can be found in the starter code, to which you can pass your JSON response to receive a simplified version. In particular, you will want to use the parsePokemonData function, which takes as a parameter the JsonObject received from pokeapi, and returns a simplified JsonObject with only the fields that are relevant for this assignment.

This simplified response will have the following information:
- baseExperienceReward – A value rewarded from combat with the pokemon
- baseStateAttack – A pokemon battle statistic
- baseStatDefense  – A pokemon battle statistic 
- baseStateMaxHp  – A pokemon battle statistic
- baseStatSpecialAttack – A pokemon battle statistic 
- baseStatSpecialDefense  – A pokemon battle statistic 
- baseStatSpeed – A pokemon battle statistic
- moves – An array containing the battle actions a pokemon can perform      
    - level_learned_at – A number defining how powerful they must be to learn the move
    - move – The name of the battle action
- pokemonNumber  – A unique numeric identifier for the pokemon
- species – The name for the kind of pokemon
- sprites – An array containing links to images of the pokemon
    - front – A link to an image showing the front of the pokemon
    - back – A link to an image showing the back of the pokemon
- types – An array containing the 1 or 2 special attributes that describe the sort of pokemon it is

## UI

The user interface for our application should be very simple. You should have a single textbox into which the user can specify either the name of a specific pokemon or it’s numeric ID, and a button labelled “search”.
When the user hits the search button, you should retrieve data about the Pokemon they entered, either via it’s numeric ID or it’s name. If that Pokemon does not exist, you should show a message declaring that the Pokemon does not exist. 
If it does exist, you should present all the above statistics about the Pokemon in a clear fashion. You must show it’s species name, number, types, battle statistics, exp value, and a full list of its moves. You must show an image of the Pokemon as well.
Your UI must remain responsive even when a request for data is in progress.


## Backend

When a search is issued, you must retrieve the data in one of two ways:
- If there is no local data for the specified Pokemon, you should launch an API request to retrieve the data from https://pokeapi.co/, as specified above. Once the data has been retrieved, you should update the appropriate UI elements to present the pokemon’s data to the user. You should additionally store all the data into a local database, from which it can be retrieved later. Upon successful retrieval of data, you should create a Toast saying “Data retrieved from web”
- If the user has requested information about the Pokemon before, you should instead retrieve the data that you stored into your local database. This data should be used to populate the appropriate UI elements, as above. Finally, upon successful retrieval of the data, you should create a Toast saying “Data restored from DB”


# Evaluation

|     Grading   Criteria      /50                                                                  |                                                                                                                                                                |   |   |   |
|--------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|---|---|---|
|     Basic   Functionality                                    /12                                 |     The   application works, providing the expected functionality without errors.   Correct use of Activities, Fragments, and Adapters when appropriate.       |   |   |   |
|     Network                                                         /12                          |     Connections   are handled appropriately; data is retrieved from online resources.   Exceptions are handled appropriately.                                  |   |   |   |
|     Database                                                                              /12    |     Retrieved   data is stored in a local database. When performing a search, local versions   are used if available. Exceptions are handled appropriately.    |   |   |   |
|     Threading                                         /12                                        |     Coroutines are used so that IO operations don’t   block the UI thread.                                                                                     |   |   |   |
|     Git Usage                                     /2                                             |     Used   an appropriate git workflow, with regular commits.                                                                                                  |   |   |   |
