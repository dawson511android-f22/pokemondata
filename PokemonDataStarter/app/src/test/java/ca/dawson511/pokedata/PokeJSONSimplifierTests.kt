package ca.dawson511.pokedata

import ca.dawson511.pokedata.data.getMoveList
import ca.dawson511.pokedata.data.parseMoveData
import ca.dawson511.pokedata.data.parsePokemonData
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import org.junit.Assert.assertEquals
import org.junit.Test
import java.io.InputStream


class PokeJSONSimplifierTests{


    private fun readTestJSON(file: String): String? {
        var json: String? = null
        try {
            val  inputStream: InputStream = this.javaClass.classLoader.getResource(file).openStream()
            json = inputStream.bufferedReader().use{it.readText()}
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    private fun parseTestJSON(file : String): JsonObject {
        return Gson().fromJson(readTestJSON(file), JsonObject::class.java)
    }


    @Test
    fun testMoveList() {
        val simplifiedMoveList : JsonArray = getMoveList(parseTestJSON("bulbasauroriginal.json"))
        val expectedMoveList : JsonArray = parseTestJSON("bulbasaur.json").getAsJsonArray("moves")
        assertEquals(simplifiedMoveList,expectedMoveList)
    }

    @Test
    fun testPokemonParses() {
        val pokemon : JsonObject = parsePokemonData(parseTestJSON("bulbasauroriginal.json"))
        val expectedPokemon : JsonObject = parseTestJSON("bulbasaur.json")
        assertEquals(pokemon,expectedPokemon)
    }

    @Test
    fun testMoveParser(){
        val move : JsonObject = parseMoveData(parseTestJSON("emberoriginal.json"))
        val expectedMove : JsonObject = parseTestJSON("ember.json")
        assertEquals(move, expectedMove)
    }

}